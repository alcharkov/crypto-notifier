package parser

import (
	"testing"

	"gitlab.com/alcharkov/crypto-notifier/model"
)

var priceChanged = false

func TestProcessPrices(t *testing.T) {

	ProcessPrices(
		model.Rules{Id: "90", Price: 100, Rule: "gt"},
		exampleApiData,
		notifyChanged)

	if !priceChanged {
		t.Errorf("Price didn't changed")
	}
}

func notifyChanged(t string) {
	priceChanged = true
}

var exampleApiData = string(
	"[{\"id\":\"90\",\"symbol\":\"BTC\",\"name\":\"Bitcoin\",\"nameid\":\"bitcoin\",\"rank\":1,\"price_usd\":\"9000\",\"percent_change_24h\":\"3.14\",\"percent_change_1h\":\"2.45\",\"percent_change_7d\":\"2.75\",\"market_cap_usd\":\"68665752557.32\",\"volume24\":\"5734357025.14\",\"volume24_native\":\"1461082.12\",\"csupply\":\"17495650.00\",\"price_btc\":\"1.00\",\"tsupply\":\"17402025\",\"msupply\":\"21000000\"}]")
