package parser

import (
	"encoding/json"
	"log"
	"strconv"

	api "gitlab.com/alcharkov/coinlore-api"
	"gitlab.com/alcharkov/crypto-notifier/model"
)

// Process prices from API response in JSON with hardcoded rules and notify passed notificators
func ProcessPrices(rule model.Rules, apiData string, notify ...func(t string)) (notified bool) {
	for _, el := range unMarshal(apiData) {
		price, err := strconv.ParseFloat(el.Price, 64)
		if err != nil {
			log.Fatal(err)
		}

		if rule.Rule == "lt" && price < rule.Price {
			return passNotifications(rule, notify)
		} else if rule.Rule == "gt" && price > rule.Price {
			return passNotifications(rule, notify)
		}

	}
	return false
}

func unMarshal(data string) []api.Crypto {
	var cryptos []api.Crypto

	err := json.Unmarshal([]byte(data), &cryptos)
	if err != nil {
		log.Fatal(err)
	}

	return cryptos
}

func passNotifications(rule model.Rules, notify []func(t string)) (notified bool) {
	for _, n := range notify {
		n(rule.Rule + " " + strconv.FormatFloat(rule.Price, 'f', 4, 64))
	}
	return true
}
