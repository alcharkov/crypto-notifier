package notifier

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/alcharkov/crypto-notifier/model"
	"gitlab.com/alcharkov/crypto-notifier/parser"
)

//Invoke price processing by seconds from configuration file
func Tick(config *model.Config, apiCallById func(string) string, writer func([]byte)) {
	for range time.Tick(config.Interval * time.Second) {
		for row, el := range config.Rules {
			if parser.ProcessPrices(el, apiCallById(el.Id), notifyConsole, notifyTelegram) {
				config.Rules = append(config.Rules[:row], config.Rules[row+1:]...)
				configBytes, err := json.Marshal(config)
				if err != nil {
					log.Fatal(err)
				}
				writer(configBytes)
			}
		}
		if !config.LoopForever {
			break
		}
	}
}

func notifyConsole(t string) {
	log.Println(t)
}

func notifyTelegram(t string) {
	_, err := http.Get(fmt.Sprintf(viper.GetString("telegram.api"), viper.GetString("telegram.token"), viper.GetString("telegram.chat"), t))
	if err != nil {
		log.Println(err)
	}
}
