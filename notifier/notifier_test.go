package notifier

import (
	"bytes"
	"testing"

	"gitlab.com/alcharkov/crypto-notifier/config"

	"github.com/spf13/viper"
)

var wrote = false

func TestWatch(t *testing.T) {
	config.Init(loadConfigFile, readInConfig)
	Tick(config.GetConfig(), exampleApiData, writer)
	if !wrote {
		t.Errorf("Config hasn't been changed")
	}
}

func loadConfigFile() {
	viper.SetConfigType("json")
}

func readInConfig() error {
	return viper.ReadConfig(bytes.NewBuffer(configExample))
}

func writer(data []byte) {
	wrote = true
}

var configExample = []byte(`
{
"interval": 1,
"loopForever": false,
"telegram": {
"api": "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s",
"token": "",
"chat": ""
},
"rules": [
{
"Id": "90",
"Price": 3470.98,
"Rule": "lt"
},
{
"Id": "90",
"Price": 3080.00,
"Rule": "gt"
}
]
}  
`)

func exampleApiData(id string) string {
	return "[{\"id\":\"90\",\"symbol\":\"BTC\",\"name\":\"Bitcoin\",\"nameid\":\"bitcoin\",\"rank\":1,\"price_usd\":\"9000\",\"percent_change_24h\":\"3.14\",\"percent_change_1h\":\"2.45\",\"percent_change_7d\":\"2.75\",\"market_cap_usd\":\"68665752557.32\",\"volume24\":\"5734357025.14\",\"volume24_native\":\"1461082.12\",\"csupply\":\"17495650.00\",\"price_btc\":\"1.00\",\"tsupply\":\"17402025\",\"msupply\":\"21000000\"}]"
}
