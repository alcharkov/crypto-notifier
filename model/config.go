package model

import "time"

type Config struct {
	Interval    time.Duration `json:"interval"`
	LoopForever bool          `json:"loopForever"`
	Rules       []Rules       `json:"rules"`
}
