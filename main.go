package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	api "gitlab.com/alcharkov/coinlore-api"

	"github.com/spf13/viper"

	"gitlab.com/alcharkov/crypto-notifier/config"
	"gitlab.com/alcharkov/crypto-notifier/notifier"
)

func main() {
	config.Init(loadConfigFile, viper.ReadInConfig)
	config.Watch()

	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	go func() {
		<-gracefulStop
		fmt.Println("Program will shutdown in 5 sec.")

		time.Sleep(5 * time.Second)
		os.Exit(0)
	}()

	notifier.Tick(config.GetConfig(), api.GetDataById, writer)
}

func loadConfigFile() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetConfigType("json")
}

func writer(data []byte) {
	err := viper.MergeConfig(bytes.NewReader(data))
	if err != nil {
		log.Fatal(err)
	}

	err = viper.WriteConfig()
	if err != nil {
		log.Fatal(err)
	}
}
