FROM golang:1.11

RUN go version

COPY . "/go/src/gitlab.com/alcharkov/crypto-notifier"
WORKDIR "/go/src/gitlab.com/alcharkov/crypto-notifier"

RUN set -x && \
    go get github.com/golang/dep/cmd/dep && \
    dep ensure -v

RUN go build -o main .
RUN chmod +x main
CMD ["./main"]