package config

import (
	"bytes"
	"testing"

	"github.com/spf13/viper"
)

func TestGetConfig(t *testing.T) {
	Init(loadConfigFile, readInConfig)

	got := GetConfig().Interval

	if got != 30 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", got, 30)
	}
}

func TestInit(t *testing.T) {

	Init(loadConfigFile, readInConfig)

	got := config
	if got == nil {
		t.Errorf("Config is wrong")
	}

}

func loadConfigFile() {
	viper.SetConfigType("json")
}

func readInConfig() error {
	return viper.ReadConfig(bytes.NewBuffer(configExample))
}

var configExample = []byte(`
{
"interval": 30,
"loopForever": true,
"telegram": {
"api": "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s",
"token": "",
"chat": ""
},
"rules": [
{
"Id": "90",
"Price": 3470.98,
"Rule": "lt"
},
{
"Id": "90",
"Price": 3080.00,
"Rule": "gt"
}
]
}  
`)
