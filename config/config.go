package config

import (
	"log"

	"github.com/fsnotify/fsnotify"

	"github.com/spf13/viper"
	"gitlab.com/alcharkov/crypto-notifier/model"
)

var (
	config       *model.Config
	configLoader func() error
)

// Get config file object
func GetConfig() *model.Config {
	return config
}

// Load config file
func Init(params func(), loader func() error) {
	params()
	configLoader = loader
	readConfig()
}

// Watch config changes
func Watch() {
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		readConfig()
	})
}

func readConfig() {
	err := configLoader()

	if err != nil {
		log.Fatal(err)
	}
	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatal(err)
	}
}
