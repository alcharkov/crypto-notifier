# crypto-notifier

## Project

###Install dep
Run in Bash
```
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```
### Project dependencies
Run in Bash
```
dep ensure
```

##Send notifications to Telegram bot
* Get Bot's token
* Send dummy message to bot
* Look for chat id in (https://api.telegram.org/botTOKEN/getUpdates) save in config